package com.chlwodh97.friendoutapi.model.gift;


import com.chlwodh97.friendoutapi.enums.GiftStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftItem {

    private Long friendId;
    private String friendName;
    private String friendPhoneNumber;


    @Enumerated(EnumType.STRING)
    private String giftStatus;
    private String content;
    private Double price;
}
