package com.chlwodh97.friendoutapi.model.gift;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftPriceChangeRequest {
    private Double price;

}
