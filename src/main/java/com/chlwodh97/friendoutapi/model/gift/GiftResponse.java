package com.chlwodh97.friendoutapi.model.gift;


import com.chlwodh97.friendoutapi.enums.GiftStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class GiftResponse {
    private Long friendId;
    private String friendName;
    private String friendPhoneNumber;

    private Long id;

    @Enumerated(EnumType.STRING)
    private String giftStatus;

    private String content;
    private Double price;
    private LocalDate giftDate;
}
