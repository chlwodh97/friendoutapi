package com.chlwodh97.friendoutapi.model.gift;


import com.chlwodh97.friendoutapi.enums.GiftStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftRequest {

    @Enumerated(EnumType.STRING)
    private GiftStatus giftStatus;

    private String content;
    private Double price;
    private LocalDate giftDate;
}
