package com.chlwodh97.friendoutapi.model.friend;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private String birth;
    private String phoneNumber;
    private String etcMemo;
    private String isCutFriend;
    private LocalDate cutDate;
    private String cutReason;
}
