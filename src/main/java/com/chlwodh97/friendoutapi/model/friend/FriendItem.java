package com.chlwodh97.friendoutapi.model.friend;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendItem {
    private String name;
    private String birth;

    private Boolean isCutFriend;

    private LocalDate cutDate;
    private String cutReason;
}
