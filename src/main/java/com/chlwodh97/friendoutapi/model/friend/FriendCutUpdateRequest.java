package com.chlwodh97.friendoutapi.model.friend;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutUpdateRequest {
    private String cutReason;
}
