package com.chlwodh97.friendoutapi.service;

import com.chlwodh97.friendoutapi.entity.Friend;
import com.chlwodh97.friendoutapi.entity.Gift;
import com.chlwodh97.friendoutapi.model.friend.*;
import com.chlwodh97.friendoutapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class FriendService {

    private final FriendRepository friendRepository;

    public Friend getData(long id){
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend (FriendRequest request) {

        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirth(request.getBirth());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutFriend(true);

        friendRepository.save(addData);

    }

    public List<FriendItem> getFriends (){

        List<Friend> originList = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();

        for (Friend friend : originList){

            FriendItem addItem = new FriendItem();
            addItem.setName(friend.getName());
            addItem.setBirth(friend.getBirth());
            addItem.setIsCutFriend(friend.getIsCutFriend());
            addItem.setCutDate(friend.getCutDate());
            addItem.setCutReason(friend.getCutReason());

           result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id) {

        Friend originData = friendRepository.findById(id).orElseThrow();

        FriendResponse response = new FriendResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBirth(originData.getBirth());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setIsCutFriend(originData.getIsCutFriend() ? "손절아님" : "손절" );        response.setCutDate(originData.getCutDate());
        response.setCutReason(originData.getCutReason());

        return response;

    }

    public void putFriendCut(long id , FriendCutUpdateRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setIsCutFriend(true);
        originData.setCutDate(LocalDate.now());
        originData.setCutReason(request.getCutReason());

        friendRepository.save(originData);
    }

    public void putFriend(long id , FriendStatusChangeRequest request) {

        Friend friend = friendRepository.findById(id).orElseThrow();

        friend.setName(request.getName());
        friend.setPhoneNumber(request.getPhoneNumber());

        friendRepository.save(friend);
    }

    public void PutFriendCutCancel(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCutFriend(false);
        originData.setCutDate(null);
        originData.setCutReason(null);

        friendRepository.save(originData);



    }



}
