package com.chlwodh97.friendoutapi.service;

import com.chlwodh97.friendoutapi.entity.Friend;
import com.chlwodh97.friendoutapi.entity.Gift;
import com.chlwodh97.friendoutapi.model.gift.GiftItem;
import com.chlwodh97.friendoutapi.model.gift.GiftPriceChangeRequest;
import com.chlwodh97.friendoutapi.model.gift.GiftRequest;
import com.chlwodh97.friendoutapi.model.gift.GiftResponse;
import com.chlwodh97.friendoutapi.repository.GiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GiftService {
    private final GiftRepository giftRepository;

    public void setGift(Friend friend, GiftRequest request) {
        Gift addData = new Gift();
        addData.setFriend(friend);
        addData.setGiftStatus(request.getGiftStatus());
        addData.setContent(request.getContent());
        addData.setPrice(request.getPrice());
        addData.setGiftDate(request.getGiftDate());

        giftRepository.save(addData);
    }

    public List<GiftItem> getGifts() {
        List<Gift> originList = giftRepository.findAll();

        List<GiftItem> result = new LinkedList<>();

        for (Gift gift : originList) {
            GiftItem addItem = new GiftItem();
            addItem.setFriendId(gift.getFriend().getId());
            addItem.setFriendName(gift.getFriend().getName());
            addItem.setGiftStatus(gift.getGiftStatus().getGive_and_take());
            addItem.setContent(gift.getContent());
            addItem.setPrice(gift.getPrice());

            result.add(addItem);

        }
        return result;
    }


    public GiftResponse getGift(long id) {
        Gift originData = giftRepository.findById(id).orElseThrow();


        GiftResponse response = new GiftResponse();

        response.setId(originData.getId());
        response.setGiftStatus(originData.getGiftStatus().getGive_and_take());
        response.setContent(originData.getContent());
        response.setPrice(originData.getPrice());
        response.setGiftDate(originData.getGiftDate());
        response.setFriendId(originData.getFriend().getId());
        response.setFriendName(originData.getFriend().getName());
        response.setFriendPhoneNumber(originData.getFriend().getPhoneNumber());

        return response;

    }

    public void putGift(long id, GiftPriceChangeRequest request) {
        Gift originData = giftRepository.findById(id).orElseThrow();
        originData.setPrice(request.getPrice());
    }

    public void delGift(long id) {
        giftRepository.deleteById(id);
    }


}

