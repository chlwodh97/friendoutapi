package com.chlwodh97.friendoutapi.controller;


import com.chlwodh97.friendoutapi.entity.Friend;
import com.chlwodh97.friendoutapi.model.gift.GiftItem;
import com.chlwodh97.friendoutapi.model.gift.GiftPriceChangeRequest;
import com.chlwodh97.friendoutapi.model.gift.GiftRequest;
import com.chlwodh97.friendoutapi.model.gift.GiftResponse;
import com.chlwodh97.friendoutapi.repository.GiftRepository;
import com.chlwodh97.friendoutapi.service.FriendService;
import com.chlwodh97.friendoutapi.service.GiftService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/gift")
public class GiftController {
    private final GiftService giftService;
    private final FriendService friendService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setGift(@PathVariable long friendId , @RequestBody GiftRequest request){
        Friend friend = friendService.getData(friendId);
        giftService.setGift(friend , request);

        return "등록되었습니당";

    }

    @GetMapping("/list/{id}")
    public List<GiftItem> getGifts(){
        return giftService.getGifts();
    }

    @PutMapping("/price-change/{id}")
    public String putGift(@PathVariable long id , @RequestBody GiftPriceChangeRequest request){
        giftService.putGift(id, request);

        return "수정할게용";
    }

    @GetMapping("/detail/{id}")
    public GiftResponse getGift(@PathVariable  long id) {
        return giftService.getGift(id);
    }

    @DeleteMapping("/del-id/{id}")
    public String delGift(@PathVariable long id){
        giftService.delGift(id);

        return "삭제되었습니다";
    }




}
