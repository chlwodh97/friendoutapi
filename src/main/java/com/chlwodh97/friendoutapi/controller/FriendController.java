package com.chlwodh97.friendoutapi.controller;


import com.chlwodh97.friendoutapi.model.friend.*;
import com.chlwodh97.friendoutapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendRequest request) {
        friendService.setFriend(request);

        return "등록 오케이";
    }

    @GetMapping("/list")
    public List<FriendItem> getFriends() {
       return friendService.getFriends();
    }

    @GetMapping("/detail/{id}")
    public FriendResponse getFriend(@PathVariable long id){
        return friendService.getFriend(id);
    }

    @PutMapping("/friend-status/{id}")
    public String putFriend(@PathVariable long id , @RequestBody FriendStatusChangeRequest request){
        friendService.putFriend(id, request);

        return "수정되었습니다";
    }

    @PutMapping("/friend-cut/{id}")
    public String putFriendCut(@PathVariable long id , @RequestBody FriendCutUpdateRequest request) {
        friendService.putFriendCut(id, request);

        return "친구 손절 완료";

    }
}
