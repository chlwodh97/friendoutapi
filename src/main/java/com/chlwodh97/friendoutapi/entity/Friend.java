package com.chlwodh97.friendoutapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Friend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String birth;

    @Column(nullable = false , unique = true)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private Boolean isCutFriend;

    private LocalDate cutDate;

    private String cutReason;
}
