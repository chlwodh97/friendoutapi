package com.chlwodh97.friendoutapi.entity;

import com.chlwodh97.friendoutapi.enums.GiftStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Gift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId" , nullable = false)
    private Friend friend;


    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private GiftStatus giftStatus;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate giftDate;
}
