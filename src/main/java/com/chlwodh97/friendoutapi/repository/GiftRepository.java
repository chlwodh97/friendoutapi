package com.chlwodh97.friendoutapi.repository;

import com.chlwodh97.friendoutapi.entity.Gift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiftRepository extends JpaRepository<Gift , Long> {
}
