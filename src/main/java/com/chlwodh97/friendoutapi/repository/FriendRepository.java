package com.chlwodh97.friendoutapi.repository;

import com.chlwodh97.friendoutapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend , Long> {
}
