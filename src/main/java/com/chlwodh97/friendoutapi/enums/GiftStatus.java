package com.chlwodh97.friendoutapi.enums;


import com.chlwodh97.friendoutapi.entity.Friend;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@RequiredArgsConstructor
public enum GiftStatus {

    GET_GIFT("받다"),
    GIVE_GIFT("주다");


    private final String give_and_take;
}
